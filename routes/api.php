<?php

use App\Http\Controllers\Api\StudentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['name' => 'api.', 'namespace' => 'App\Http\Controllers\Api'], function () use (&$api) {
    $api->group(['prefix' => 'students', 'name' => 'students.'], function () use (&$api) {
        $api->get('', 'StudentController@index')->name('index');
        $api->get('{student}', 'StudentController@show')->name('view');
        $api->post('', 'StudentController@store')->name('store');
        $api->post('{student}', 'StudentController@update')->name('update');
        $api->delete('{student}', 'StudentController@destroy')->name('delete');
    });

    $api->group(['prefix' => 'groups', 'name' => 'groups.'], function () use (&$api) {
        $api->get('', 'GroupController@index')->name('index');
        $api->get('{group}', 'GroupController@show')->name('view');
        $api->post('', 'GroupController@store')->name('store');
        $api->post('{group}', 'GroupController@update')->name('update');
        $api->delete('{group}', 'GroupController@destroy')->name('delete');

        $api->group(['prefix' => '{group}/syllabus', 'name' => 'syllabus.'], function () use (&$api) {
            $api->get('', 'SyllabusController@show')->name('view');
            $api->post('', 'SyllabusController@store')->name('store');
        });
    });

    $api->group(['prefix' => 'lectures', 'name' => 'lectures.'], function () use (&$api) {
        $api->get('', 'LectureController@index')->name('index');
        $api->get('{lecture}', 'LectureController@show')->name('view');
        $api->post('', 'LectureController@store')->name('store');
        $api->post('{lecture}', 'LectureController@update')->name('update');
        $api->delete('{lecture}', 'LectureController@destroy')->name('delete');
    });
});

