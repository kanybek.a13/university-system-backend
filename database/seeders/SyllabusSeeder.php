<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\Lecture;
use App\Models\Syllabus;
use App\Models\SyllabusLecture;
use Illuminate\Database\Seeder;

class SyllabusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groups = Group::all();
        $lectures = Lecture::all();

        foreach ($groups as $group) {
            $syllabus = Syllabus::factory()
                ->create([
                    'group_id' => $group->id,
                ]);

               $lectures->random(5)->each(function ($lecture) use (&$syllabus) {
                    SyllabusLecture::create([
                        'lecture_id' => $lecture->id,
                        'syllabus_id' => $syllabus->id,
                        'time' => now(),
                     ]);
                });
        }
    }
}
