<?php

namespace Database\Seeders;

use App\Models\Group;
use App\Models\Student;
use Illuminate\Database\Seeder;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $groupIds = Group::select('id')->get();

        foreach ($groupIds as $groupId) {
            Student::factory()
                ->count(10)
                ->create([
                    'group_id' => $groupId,
                ]);
        }
    }
}
