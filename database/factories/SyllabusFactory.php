<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class SyllabusFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'group_id' => null,
            'name' => $this->faker->title
        ];
    }
}
