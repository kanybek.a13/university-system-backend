<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\SyllabusLecture
 *
 * @property int $id
 * @property int $syllabus_id
 * @property int $lecture_id
 * @property string $time
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|SyllabusLecture newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SyllabusLecture newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|SyllabusLecture query()
 * @method static \Illuminate\Database\Eloquent\Builder|SyllabusLecture whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SyllabusLecture whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SyllabusLecture whereLectureId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SyllabusLecture whereSyllabusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SyllabusLecture whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|SyllabusLecture whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SyllabusLecture extends Model
{
    use HasFactory;

    protected $table = 'syllabus_lecture';

    protected $fillable = ['syllabus_id', 'lecture_id', 'time'];

}
