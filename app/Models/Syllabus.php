<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Models\Syllabus
 *
 * @property int $id
 * @property int $group_id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\Models\Group $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Lecture[] $lectures
 * @property-read int|null $lectures_count
 * @method static \Database\Factories\SyllabusFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Syllabus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Syllabus newQuery()
 * @method static \Illuminate\Database\Query\Builder|Syllabus onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Syllabus query()
 * @method static \Illuminate\Database\Eloquent\Builder|Syllabus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Syllabus whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Syllabus whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Syllabus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Syllabus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Syllabus whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Syllabus withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Syllabus withoutTrashed()
 * @mixin \Eloquent
 */
class Syllabus extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'syllabus';

    protected $fillable = ['group_id', 'time'];

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id', 'id');
    }

    public function lectures()
    {
        return $this->belongsToMany(Lecture::class, 'syllabus_lecture')
            ->orderBy('syllabus_lecture.time', 'asc')
            ->withPivot('time')
            ->withTimestamps();
    }
}
