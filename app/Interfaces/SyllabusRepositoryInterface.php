<?php

namespace App\Interfaces;

interface SyllabusRepositoryInterface
{
    public function getAllSyllabuses();
    public function getSyllabusById($id);
    public function getSyllabusByGroupId($groupId);
    public function storeSyllabus($groupId, array $details);
}
