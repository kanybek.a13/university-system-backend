<?php

namespace App\Interfaces;

interface LectureRepositoryInterface
{
    public function getAllLectures();
    public function getLectureById($lectureId);
    public function deleteLecture($lectureId);
    public function createLecture(array $lectureDetails);
    public function updateLecture($lectureId, array $newDetails);
}
