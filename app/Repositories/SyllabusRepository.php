<?php

namespace App\Repositories;

use App\Interfaces\SyllabusRepositoryInterface;
use App\Models\Syllabus;
use App\Models\SyllabusLecture;
use Illuminate\Support\Arr;

class SyllabusRepository implements SyllabusRepositoryInterface
{
    public function getAllSyllabuses()
    {
        return Syllabus::all();
    }

    public function getSyllabusById($groupId)
    {
        return Syllabus::whereId($groupId)
            ->with(['lectures'])
            ->firstOrFail();
    }

    public function getSyllabusByGroupId($groupId)
    {
        return Syllabus::query()
            ->whereGroupId($groupId)
            ->firstOrFail();
    }

    public function storeSyllabus($groupId, array $details)
    {
        $syllabus = Syllabus::updateOrCreate(
            [
                'group_id' => $groupId,
            ],
            [
                'group_id' => $groupId,
                'name' => $details['name']
            ]);

        $toDeleteIds = array_diff($syllabus->lectures()->pluck('lecture_id')->toArray(), Arr::pluck($details['lectures'], 'id'));

        foreach ($details['lectures'] as $lecture) {
            SyllabusLecture::updateOrCreate(
                [
                    'syllabus_id' => $syllabus->id,
                    'lecture_id' => $lecture['id']
                ],
                [
                    'syllabus_id' => $syllabus->id,
                    'lecture_id' => $lecture['id'],
                    'time' => $lecture['time']
                ]);
        }

        SyllabusLecture::query()
            ->whereSyllabusId($syllabus->id)
            ->whereIn('lecture_id', $toDeleteIds)
            ->delete();

        return $syllabus;
    }
}
