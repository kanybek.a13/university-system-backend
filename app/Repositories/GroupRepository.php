<?php

namespace App\Repositories;

use App\Interfaces\GroupRepositoryInterface;
use App\Models\Group;

class GroupRepository implements GroupRepositoryInterface
{
    public function getAllGroups()
    {
        return Group::all();
    }

    public function getGroupById($groupId)
    {
        return Group::whereId($groupId)
            ->with(['syllabus', 'students'])
            ->firstOrFail();
    }

    public function deleteGroup($groupId)
    {
        return Group::findOrFail($groupId)->delete();
    }

    public function createGroup(array $groupDetails)
    {
        return Group::create($groupDetails);
    }

    public function updateGroup($groupId, array $newDetails)
    {
        $group = $this->getGroupById($groupId);

        $group->update($newDetails);

        return $group;
    }
}
