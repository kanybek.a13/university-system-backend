<?php

namespace App\Repositories;

use App\Interfaces\StudentRepositoryInterface;
use App\Models\Student;

class StudentRepository implements StudentRepositoryInterface
{
    public function getAllStudents()
    {
        return Student::all();
    }

    public function getStudentById($studentId)
    {
        return Student::whereId($studentId)
            ->with(['group'])
            ->firstOrFail();
    }

    public function deleteStudent($studentId)
    {
        return Student::findOrFail($studentId)->delete();
    }

    public function createStudent(array $studentDetails)
    {
        return Student::create($studentDetails);
    }

    public function updateStudent($studentId, array $newDetails)
    {
        $student = $this->getStudentById($studentId);

        $student->update($newDetails);

        return $student;
    }
}
