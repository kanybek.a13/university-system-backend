<?php

namespace App\Repositories;

use App\Interfaces\LectureRepositoryInterface;
use App\Models\Lecture;

class LectureRepository implements LectureRepositoryInterface
{
    public function getAllLectures()
    {
        return Lecture::all();
    }

    public function getLectureById($lectureId)
    {
        return Lecture::whereId($lectureId)
            ->with(['groups', 'groups.students'])
            ->firstOrFail();
    }

    public function deleteLecture($lectureId)
    {
        return Lecture::findOrFail($lectureId)->delete();
    }

    public function createLecture(array $lectureDetails)
    {
        return Lecture::create($lectureDetails);
    }

    public function updateLecture($lectureId, array $newDetails)
    {
        $lecture = $this->getLectureById($lectureId);

        $lecture->update($newDetails);

        return $lecture;
    }
}
