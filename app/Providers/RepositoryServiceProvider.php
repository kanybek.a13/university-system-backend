<?php

namespace App\Providers;

use App\Interfaces\GroupRepositoryInterface;
use App\Interfaces\LectureRepositoryInterface;
use App\Interfaces\StudentRepositoryInterface;
use App\Interfaces\SyllabusRepositoryInterface;
use App\Repositories\GroupRepository;
use App\Repositories\LectureRepository;
use App\Repositories\StudentRepository;
use App\Repositories\SyllabusRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StudentRepositoryInterface::class, StudentRepository::class);
        $this->app->bind(GroupRepositoryInterface::class, GroupRepository::class);
        $this->app->bind(LectureRepositoryInterface::class, LectureRepository::class);
        $this->app->bind(SyllabusRepositoryInterface::class, SyllabusRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
