<?php

namespace App\Providers;

use App\Models\Group;
use App\Observers\GroupObserver;
use Illuminate\Support\ServiceProvider;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        Group::observe(GroupObserver::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Group::observe(GroupObserver::class);
    }
}
