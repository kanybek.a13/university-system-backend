<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GroupResource extends JsonResource
{
    /**
     * @param $request
     * @param $extended
     * @return array|array[]|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
        ];

        if ($this->extended) {
            $lectures = [];

            if ($this->syllabus) {
                foreach ($this->syllabus->lectures as $lecture) {
                    $lectures[] = [
                        'id' => $lecture->id,
                        'topic' => $lecture->topic,
                    ];
                }
            }

            $students = [];
            foreach ($this->students as $student) {
                $students[] = [
                    'id' => $student->id,
                    'name' => $student->name,
                ];
            }

            $data = array_merge($data, [
                'lectures' => $lectures,
                'students' => $students,
            ]);
        }

        return $data;
    }
}
