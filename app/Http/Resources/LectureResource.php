<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class LectureResource extends JsonResource
{
    /**
     * @param $request
     * @param $extended
     * @return array|array[]|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request, $extended = false)
    {
        $data = [
            'id' => $this->id,
            'topic' => $this->topic,
        ];

        if ($this->extended) {
            $groups = [];
            $students = [];

            foreach ($this->groups as $group) {
                $groups[] = [
                    'id' => $group->id,
                    'name' => $group->name,
                ];

                foreach ($group->students as $student) {
                    $students[] = [
                        'id' => $student->id,
                        'name' => $student->name,
                    ];
                }
            }

            $data = array_merge($data, [
                'description' => $this->description,
                'groups' => $groups,
                'students' => $students,
            ]);
        }

        return $data;
    }
}
