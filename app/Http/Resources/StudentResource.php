<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * @param $request
     * @param $extended
     * @return array|array[]|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request, $extended = false)
    {
        $data = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'group' => $this->group ? [
                'id' => $this->group->id,
                'name' => $this->group->name
            ] : null,
        ];

        if ($this->extended) {
            $lectures = [];

            if ($this->group->syllabus) {
                foreach ($this->group->syllabus->lectures as $lecture) {
                    $lectures[] = [
                        'id' => $lecture->id,
                        'topic' => $lecture->topic,
                    ];
                }
            }

            $data = array_merge($data, [
                'lectures' => $lectures,
            ]);
        }

        return $data;
    }
}
