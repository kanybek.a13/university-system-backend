<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SyllabusResourse extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $lectures = [];
        foreach ($this->lectures as $lecture) {
            $lectures[] = [
                'id' => $lecture->id,
                'topic' => $lecture->topic,
            ];
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'lectures' => $lectures,
        ];
    }
}
