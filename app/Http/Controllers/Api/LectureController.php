<?php

namespace App\Http\Controllers\Api;

use App\Http\Classes\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\LectureStoreRequest;
use App\Http\Resources\LectureResource;
use App\Interfaces\LectureRepositoryInterface;
use Illuminate\Http\JsonResponse;

class LectureController extends Controller
{
    /**
     * @var LectureRepositoryInterface
     */
    private LectureRepositoryInterface $lectureRepository;

    /**
     * @param LectureRepositoryInterface $lectureRepository
     */
    public function __construct(LectureRepositoryInterface $lectureRepository)
    {
        $this->lectureRepository = $lectureRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $lectures = $this->lectureRepository->getAllLectures();

        $message = new Message('Lectures fetched', LectureResource::collection($lectures));
        return response()->json($message);
    }

    /**
     * @param LectureStoreRequest $request
     * @return JsonResponse
     */
    public function store(LectureStoreRequest $request): JsonResponse
    {
        $data = $request->validated();

        $lecture = $this->lectureRepository->createLecture($data);
        $lecture->extended = true;

        $message = new Message('Lecture created successfully', new LectureResource($lecture));
        return response()->json($message);
    }

    /**
     * @param $lectureId
     * @return JsonResponse
     */
    public function show($lectureId): JsonResponse
    {
        $lecture = $this->lectureRepository->getLectureById($lectureId);

        $lecture->extended = true;

        $message = new Message('Lecture fetched successfully', new LectureResource($lecture));
        return response()->json($message);
    }

    /**
     * @param LectureStoreRequest $request
     * @param $lectureId
     * @return JsonResponse
     */
    public function update(LectureStoreRequest $request, $lectureId): JsonResponse
    {
        $data = $request->validated();

        $lecture = $this->lectureRepository->updateLecture($lectureId, $data);
        $lecture->extended = true;

        $message = new Message('Lecture updated successfully', new LectureResource($lecture));
        return response()->json($message);
    }

    /**
     * @param $lectureId
     * @return JsonResponse
     */
    public function destroy($lectureId): JsonResponse
    {
        $this->lectureRepository->deleteLecture($lectureId);

        $message = new Message('Lecture deleted successfully');
        return response()->json($message);
    }
}
