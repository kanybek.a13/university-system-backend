<?php

namespace App\Http\Controllers\Api;

use App\Http\Classes\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\GroupStoreRequest;
use App\Http\Resources\GroupResource;
use App\Interfaces\GroupRepositoryInterface;
use Illuminate\Http\JsonResponse;

class GroupController extends Controller
{
    /**
     * @var GroupRepositoryInterface
     */
    private GroupRepositoryInterface $groupRepository;

    /**
     * @param GroupRepositoryInterface $groupRepository
     */
    public function __construct(GroupRepositoryInterface $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $groups = $this->groupRepository->getAllGroups();

        $message = new Message('Groups fetched', GroupResource::collection($groups));
        return response()->json($message);
    }

    /**
     * @param GroupStoreRequest $request
     * @return JsonResponse
     */
    public function store(GroupStoreRequest $request): JsonResponse
    {
        $data = $request->validated();

        $group = $this->groupRepository->createGroup($data);
        $group->extended = true;

        $message = new Message('Group created successfully', new GroupResource($group));
        return response()->json($message);
    }

    /**
     * @param $groupId
     * @return JsonResponse
     */
    public function show($groupId): JsonResponse
    {
        $group = $this->groupRepository->getGroupById($groupId);
        $group->extended = true;

        $message = new Message('Group fetched successfully', new GroupResource($group));
        return response()->json($message);
    }

    /**
     * @param GroupStoreRequest $request
     * @param $groupId
     * @return JsonResponse
     */
    public function update(GroupStoreRequest $request, $groupId): JsonResponse
    {
        $data = $request->validated();

        $group = $this->groupRepository->updateGroup($groupId, $data);
        $group->extended = true;

        $message = new Message('Group updated successfully', new GroupResource($group));
        return response()->json($message);
    }

    /**
     * @param $groupId
     * @return JsonResponse
     */
    public function destroy($groupId): JsonResponse
    {
        $this->groupRepository->deleteGroup($groupId);

        $message = new Message('Group deleted successfully');
        return response()->json($message);
    }
}
