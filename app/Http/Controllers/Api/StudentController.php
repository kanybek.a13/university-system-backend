<?php

namespace App\Http\Controllers\Api;

use App\Http\Classes\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\StudentStoreRequest;
use App\Http\Requests\StudentUpdateRequest;
use App\Http\Resources\StudentResource;
use App\Interfaces\StudentRepositoryInterface;
use Illuminate\Http\JsonResponse;

class StudentController extends Controller
{
    /**
     * @var StudentRepositoryInterface
     */
    private StudentRepositoryInterface $studentRepository;

    /**
     * @param StudentRepositoryInterface $studentRepository
     */
    public function __construct(StudentRepositoryInterface $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $students = $this->studentRepository->getAllStudents();

        $message = new Message('Students fetched', StudentResource::collection($students));
        return response()->json($message);
    }

    /**
     * @param StudentStoreRequest $request
     * @return JsonResponse
     */
    public function store(StudentStoreRequest $request): JsonResponse
    {
        $data = $request->validated();

        $student = $this->studentRepository->createStudent($data);
        $student->extended = true;

        $message = new Message('Student created successfully', new StudentResource($student));
        return response()->json($message);
    }

    /**
     * @param $studentId
     * @return JsonResponse
     */
    public function show($studentId): JsonResponse
    {
        $student = $this->studentRepository->getStudentById($studentId);
        $student->extended = true;

        $message = new Message('Student fetched successfully', new StudentResource($student));
        return response()->json($message);
    }

    /**
     * @param StudentUpdateRequest $request
     * @param $studentId
     * @return JsonResponse
     */
    public function update(StudentUpdateRequest $request, $studentId): JsonResponse
    {
        $data = $request->validated();

        $student = $this->studentRepository->updateStudent($studentId, $data);
        $student->extended = true;

        $message = new Message('Student updated successfully', new StudentResource($student));
        return response()->json($message);
    }

    /**
     * @param $studentId
     * @return JsonResponse
     */
    public function destroy($studentId): JsonResponse
    {
        $this->studentRepository->deleteStudent($studentId);

        $message = new Message('Student deleted successfully');
        return response()->json($message);
    }
}
