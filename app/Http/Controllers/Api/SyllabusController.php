<?php

namespace App\Http\Controllers\Api;

use App\Http\Classes\Message;
use App\Http\Controllers\Controller;
use App\Http\Requests\SyllabusStoreRequest;
use App\Http\Resources\SyllabusResourse;
use App\Interfaces\SyllabusRepositoryInterface;

class SyllabusController extends Controller
{
    /**
     * @var SyllabusRepositoryInterface
     */
    private SyllabusRepositoryInterface $syllabusRepository;

    /**
     * @param SyllabusRepositoryInterface $syllabusRepository
     */
    public function __construct(SyllabusRepositoryInterface $syllabusRepository)
    {
        $this->syllabusRepository = $syllabusRepository;
    }

    /**
     * @param $groupId
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($groupId)
    {
        $syllabus = $this->syllabusRepository->getSyllabusByGroupId($groupId);

        $message = new Message('Syllabus fetched successfully', new SyllabusResourse($syllabus));
        return response()->json($message);
    }

    /**
     * @param SyllabusStoreRequest $request
     * @param $groupId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SyllabusStoreRequest $request, $groupId)
    {
        $data = $request->validated();

        $syllabus = $this->syllabusRepository->storeSyllabus($groupId, $data);

        $message = new Message('Syllabus created successfully', new SyllabusResourse($syllabus));
        return response()->json($message);
    }
}
