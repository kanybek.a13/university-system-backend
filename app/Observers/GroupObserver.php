<?php

namespace App\Observers;

use App\Models\Group;
use App\Models\Student;

class GroupObserver
{
    public function deleted(Group $item)
    {
        Student::whereGroupId($item->id)
            ->update([
                'group_id' => null
            ]);
    }
}
